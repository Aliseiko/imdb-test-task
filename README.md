# Description
Set of test cases to part of Top 250 IMDb movies page.
Based on:
* Python > 3.7
* [Playwright library](https://github.com/microsoft/playwright-python)
* Pytest
* Page object pattern

# Usage
## Get dependencies
All dependencies are handled by docker container. You can either use pre-build `testautomationci/playwright-python` image or build your own using Dockerfile from this repository:
* Get pre-build image:
```
docker pull testautomationci/playwright-python
```
* Build your own image:
```
docker build -t playwright-python .
```

IMPORTANT NOTE: Image is based on official playwright image from Microsoft and quite fat so initial image pull may take some time. Currently it's not impossible to use thin Alpine-based images due to libc  / musl incompatability.

## Run
Simply run below command from project's root folder. Replace `testautomationci/playwright-python` with `playwright-python` if you use locally built image.

```
docker run -it --rm -v `pwd`:/root/work -w /root/work -e PYTHONPATH=/root/work testautomationci/playwright-python pytest --log-cli-level=10
```

## Multibrowser support
Browser type is driving by `--browser` pytest option. If not set Chromium is used by default. Supprted types: `chromium`, `firefox` and `webkit`

E.g. you may run the tests under firefox:
```
docker run -it --rm -v `pwd`:/root/work -w /root/work -e PYTHONPATH=/root/work testautomationci/playwright-python pytest --log-cli-level=10 --browser firefox
```


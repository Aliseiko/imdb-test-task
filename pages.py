from playwright.sync_api import Page, ElementHandle
import typing
from dataclasses import dataclass
import re
from logger import LOGGER


class AbstractPage:

    url = ''

    def __init__(self, page: Page):
        LOGGER.debug(f"Initializing {type(self).__name__}")
        self.page = page
        self.page.waitForLoadState(state="load")

    def open(self):
        """Opens page in browser using pre-defined page url
        Returns:
            self: instance of the current page
        """
        self.page.goto(self.url)
        return self

    @property
    def page_title(self) -> str:
        return self.page.title()


@dataclass
class MovieRow:
    rank: int
    name: str
    year: int
    imbd_rate: float
    raw_element: ElementHandle


class Top250MoviesPage(AbstractPage):

    url = "https://www.imdb.com/chart/top?ref_=nv_mv_250_6"

    movies_list_locator = '//table[@data-caller-name="chart-top250movie"]/tbody/tr'
    sortby_dropdown_locator = '//select[@id="lister-sort-by-options"]'
    movie_details_locator = '//*[@class="{}Column"]/a'

    def get_movies_list(self) -> typing.List[MovieRow]:
        """Get movie list from chart. Parses each movie element using regexp and gets details: rank, name, year, imbd_rate

        Return (list):
            Ordered list of MovieRow instances. List represents movies chart

        """
        LOGGER.debug("Getting list of movies")
        regexp = r"(\d+)\. (.+) \((\d+)\)\s+([0-9\.]+)"
        movies_raw_list = self.page.querySelectorAll(self.movies_list_locator)
        movies_list = []
        for movie_object in movies_raw_list:
            res = re.findall(regexp, movie_object.innerText())
            # Example innter text: 1. The Shawshank Redemption (1994)      9.2
            # Example res: [('246', 'The Terminator', '1984', '8.0')]
            rank = int(res[0][0])
            name = res[0][1]
            year = int(res[0][2])
            imbd_rate = float(res[0][3])
            movies_list.append(MovieRow(rank, name, year, imbd_rate, movie_object))
        return movies_list

    def goto_movie_details(self, movie_name, where="title"):
        """Navigates to the given movie details page
        Args:

            movie_name (str): Movie name navigate to
            where (str): could be `title` or `poster` -
                this is where click will be done on poster or on movie name
        Returns:
            MovieDetailsPage: Instance of MovieDetailsPage
        """
        LOGGER.debug(f"Clicking on movie {movie_name} by {where}")
        movies = self.get_movies_list()
        movie = [movie for movie in movies if movie.name == movie_name][0]
        movie.raw_element.querySelector(self.movie_details_locator.format(where)).click()
        return MovieDetailsPage(self.page)

    def set_sorting(self, sorting_option: str):
        """Allow to set sorting method for the top 250 chart

        Args:

            sorting_option (str): Sorting type need to be set.
            Possible values: Ranking, IMDb Rating, Number of Ratings, Your Rating
        """
        LOGGER.debug(f"Setting sorting for chart by {sorting_option}")
        self.page.selectOption(self.sortby_dropdown_locator, values={"label": sorting_option})
        return self


class MovieDetailsPage(AbstractPage):
    pass

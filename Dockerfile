FROM mcr.microsoft.com/playwright:latest

USER root
COPY ./requirements.txt /
RUN apt-get update \
    && apt-get install -y --no-install-recommends software-properties-common python3-pip \
    && add-apt-repository ppa:deadsnakes/ppa \
    && apt-get install -y --no-install-recommends python3.7 \
    && update-alternatives --install /usr/bin/python3 python3 /usr/bin/python3.7 1 \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*
RUN pip3 install -r requirements.txt && python3 -m playwright install


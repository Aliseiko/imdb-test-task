import pytest
from pages import Top250MoviesPage


@pytest.fixture(scope="module")
def top_250_page(browser):
    page = browser.newPage()
    top_page = Top250MoviesPage(page)
    top_page.open()
    return top_page


def test_set_sort_by_IMDb_rating(top_250_page: Top250MoviesPage):
    imdb_sorted_movies_lst = top_250_page.set_sorting("IMDb Rating").get_movies_list()
    previous_movie_rate = imdb_sorted_movies_lst[0].imbd_rate
    for movie in imdb_sorted_movies_lst:
        assert movie.imbd_rate <= previous_movie_rate, \
                f"Lower movies must have equal or less IMDb rate when sorted by IMDb rating. \
                Previous(higer) movie has rate {previous_movie_rate}, current {movie.imbd_rate} "
        previous_movie_rate = movie.imbd_rate


def test_set_sort_by_ranking(top_250_page: Top250MoviesPage):
    imdb_sorted_movies_lst = top_250_page.set_sorting("Ranking").get_movies_list()
    previous_movie_rank = 0
    for movie in imdb_sorted_movies_lst:
        assert movie.rank > previous_movie_rank, \
                f"Lower movies must greater rank when sorted by Ranking. \
                Previous(higer) movie had rate {previous_movie_rank}, current {movie.rank}"
        previous_movie_rank = movie.rank


@pytest.mark.parametrize("click_on", ["poster", "title"])
def test_chart_movie_title_and_cover_are_clickable(top_250_page: Top250MoviesPage, click_on):
    movie_name = "The Shawshank Redemption"
    top_250_page.open()
    detailed_movie_page = top_250_page.goto_movie_details(movie_name, where=click_on)
    detaild_page_title = detailed_movie_page.page_title
    assert movie_name in detaild_page_title, f"Wrong title on detailed page for {movie_name} \
                                               when clicked on movie {click_on}. Actual title {detaild_page_title}"
